import React, {Component} from 'react';
import { Card, Button } from 'semantic-ui-react';
import factory from '../ethereum/factory';
import Layout from '../components/Layout';
import { Link } from '../routes';

// const campaignIndex = new CampaignIndex();

class CampaignIndex extends Component {
    static async getInitialProps(){

    const campaigns = await factory.methods.getDeployedCampaigns().call();

    return {campaigns};

    }

    renderCampaigns() {
        const items = this.props.campaigns.map(address => {
            return {
                header:address,
                description: (
                    <Link route={`/campaigns/${address}`}>
                    <a>View VID Charge Events</a>
                    </Link>
                ),
                fluid: true
            };

        });
        return <Card.Group items={items} />;
        // Pass a function into a map, and that function will be passed in
        // with each element into the array.
        // That will be assigned into each element of trhe array.
    }

 //   async componentDidMount() {
        // Returns of all our deployed addresses except 1.
      //  const campaigns = await factory.methods.getDeployedCampaigns().call();
   //     console.log(campaigns);
    //}

    // To make react happy, you need to output some JSX!!

    render() {
        return (
            <Layout>
            <div>

                <h3> Open Campaigns </h3>

                    <Link route="/campaigns/new">
                        <a>
                        <Button
                            floated = "right"
                            content="Create Campaign"
                        icon="add circle"
                        primary
                        />
                        </a>
                    </Link>

                {this.renderCampaigns()}
            </div>
        </Layout>
        );
        // Return the first campagin stored in the campaigns array.
    }

}

export default CampaignIndex;
