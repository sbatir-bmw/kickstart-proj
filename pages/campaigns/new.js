import React, { Component }from 'react';
import {Form, Button, Input, Message} from 'semantic-ui-react';
import Layout from '../../components/Layout';
import factory from '../../ethereum/factory';
import web3 from '../../ethereum/web3';
import { Router } from '../../routes';

class CampaignNew extends Component {
    state = {
        minimumContribution: '',
        errorMessage: '', // allows u to assign an attribute known as "errorMesasage" to track
        // abnormal user inputs.... then changehe
        loading: false
    };

    onSubmit = async (event) => {
        event.preventDefault();
        this.setState({loading: true, errorMessage: ''});
// Lec 170: Introduce a try/catch in case there are inappropriate inserts to deploy a new campaign

        try {

        const accounts = await web3.eth.getAccounts();
        await factory.methods
            .createCampaign(this.state.minimumContribution)
            .send({
                from: accounts[0]
            });

        Router.pushRoute('/');
    } catch (err) { // goes with the try statement.
            this.setState({ errorMessage: err.message});

        }
        // After the state is completed, return the loading part to "false"
        this.setState({loading: false});
    };
    render() {
        return(
        <Layout>

        <h1> New Campaign!</h1>
            <Form onSubmit ={this.onSubmit} error={!!this.state.errorMessage} >
                <Form.Field>
                    <label>Minimum Contribution</label>
                    <Input
                        label="wei"
                        labelPosition = "right"
                        value={this.state.minimumContribution}
                        onChange={event => this.setState({ minimumContribution: event.target.value })}
                    />

                </Form.Field>
                <Message error  header="An error has occurred!" content={this.state.errorMessage} />
                <Button loading = {this.state.loading} primary> Create! </Button>

            </Form>
        </Layout>
        );
    }
}
// Observe! When you use the Message object from Semantic UI, it does not show up
// by default. If you want, then need to add property "error" in enclosing form.
export default CampaignNew;
