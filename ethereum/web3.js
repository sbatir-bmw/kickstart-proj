import Web3 from 'web3';

let web3; // declares and sets up this variable for future suse.
// the widnow.web3 section fails and breaks on server side version.

if (typeof window !== "undefined" && typeof window.web3 !== 'undefined') { // typeof is used to see if variable is defined.
    // if in browser and metamask is running
    web3 = new Web3(window.web3.currentProvider); // hijack this thing's provider.
    // if you're in node.js runtime, the variable is undefined.
    // You'll return "object" for the type of window if you're in the browser side.
} else {
    // IF...we are on the server, OR user has no metamask.
    // set up our own provider that connects to the test network through infura.
    const provider = new Web3.providers.HttpProvider(
'https://rinkeby.infura.io/v3/3ed05ab005bf4d85a60e6264de5574f7'
    );
    web3 = new Web3(provider);
    // This line here enables you to pass into the new provider that we passed htorugh our unique access key via the
    // Infura portal into the rinkeby test network.

}
// const web3 = new Web3(window.web3.currentProvider);
// window.web3.currentProvider is the provider that is given to us by
// metamask.
// This will throw an error for people without metamask.


// Then you need to make sure that you are exporting this!

export default web3;