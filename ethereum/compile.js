const path = require('path');
const solc = require('solc');
const fs = require('fs-extra'); // community made extra.... an improved verison of fs module but saves us additional time

const buildPath = path.resolve(__dirname, 'build'); // steps out one directory _ _ directory
// call fs module to remove a directory
fs.removeSync(buildPath); // removeSync is from fs-extra (communit dev)

const campaignPath = path.resolve(__dirname, 'contracts', 'Campaign.sol');
// defines path to contract directory.

const source = fs.readFileSync(campaignPath, 'utf8');
const output = solc.compile(source, 1).contracts;
// pulls contracts property and assigns it to output.

fs.ensureDirSync(buildPath);
// ensureDirSync creates a new build folder.

console.log(output); // We know output comes form the solidity compiler "solc"
for (let contract in output) {
    fs.outputJsonSync(
        path.resolve(buildPath, contract.replace(':','') + '.json'),
        output[contract]
    );
}