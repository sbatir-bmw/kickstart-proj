import web3 from './web3';
import CampaignFactory from './build/CampaignFactory.json';

const instance = new web3.eth.Contract(
    // Your contract instance lives in the ABI, which is this .JSON above.
    JSON.parse(CampaignFactory.interface),
    '0xAfF066Eafd89fcA427b7dF41eC88417eF4183652'
);

export default instance;

// Why separate some code between web3.js and factory.js?
// If you need to get access to your deployed factory instance, from
// somewhere else inside the application, then you won't have to go through
// the entire process of importing web3.js , the interface, and get the addrewss and all thaat stuff.
// Now, latency is not laggy.
// Instead, to get handl eon insance, you just import the factory.js file.