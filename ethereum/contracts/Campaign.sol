pragma solidity ^0.4.17;


contract CampaignFactory {
    address[] public deployedCampaigns;

    function createCampaign(uint minimum) public {
        // takes one argument
        // instruct this contract to create a new contract.
        address newCampaign = new Campaign(minimum, msg.sender);
        deployedCampaigns.push(newCampaign);

    }

    function getDeployedCampaigns() public view returns (address[]) {
        return deployedCampaigns;
    }
}
contract Campaign {
    struct Request {
        string description; // need sep. with semi-colon.
        uint value;
        address recipient;
        bool complete;
        uint approvalCount;
        mapping (address => bool) approvals;
    }

    modifier restricted() {
        require(msg.sender == manager);
        _;
    }
    Request[] public requests; // Creates a request array of type
    address public manager;
    uint public minimumContribution;
    mapping(address => bool) public approvers;
    uint public approversCount;

    function Campaign(uint minimum, address creator) public {
        manager = creator;
        minimumContribution = minimum;
    }

    function contribute() public payable {
        require(msg.value > minimumContribution);
        // msg.value is the user input that is targeting this function "contribute"
        // This line is actually obsolete because push is for arrays.
        //approvers.push(msg.sender);
        approvers[msg.sender] = true;
        // Setting the mapping to "true" means that they ahve been included.
        // The address of hte person who is sending this transaction is the ".sender" property
        // found within the struct "msg"
        approversCount++;
        //requests[0];

    }

    function createRequest(string description, uint value, address recipient) public restricted {
        Request memory newRequest = Request({
           description: description,
           value: value,
           recipient: recipient,
           complete: false,
           approvalCount: 0
        });

        requests.push(newRequest);
    }

    function approveRequest(uint index) public {
        Request storage request = requests[index];
        require(approvers[msg.sender]); // Require u get bool True.  <
       // removing l.55 fixed the approveRequest bug... is that OK?
        require(!request.approvals[msg.sender]);
        // The not kicks user out if it's bool is False.
        request.approvals[msg.sender] = true;
        request.approvalCount++;
    } /*
    function approveRequest(Request request) public {
        // Ensure personc alling function has donated..
        // sets cost at 10,000 gas per person
        bool isApprover = false;
        for (uint i=0; i < approvers.length; i++) {
            if (approvers[i] == msg.sender) {
                isApprover = true;
            }
        }
        require(isApprover);
        //make sure caller person hasn't voted already.
        // cost is 5000 gas per person.
        for (uint i = 0; i < request.approvers.length; i++) {
            require(approvers != msg.sender);
        }
    }
    */
    function finalizeRequest(uint index) public restricted {
        // Refers to request struct
        Request storage request = requests[index];
        require(request.approvalCount > (approversCount / 2));
        require(!requests[index].complete);
        // remember that recipient is an address. Intrinsically packaged with ".transfer" op.
        request.recipient.transfer(request.value);
        requests[index].complete = true;
    }

    function getSummary() public view returns (
    uint, uint, uint, uint, address
    ) {
        return (
        minimumContribution,
        this.balance,
        requests.length,
        approversCount,
        manager
    );
    }

    function getRequestsCount() public view returns (uint) {
        return requests.length;
    }


}