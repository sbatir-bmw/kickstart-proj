const HDWalletProvider = require('truffle-hdwallet-provider');
const Web3 = require('web3');
const compiledFactory = require('./build/CampaignFactory.json');
// fix the compile part of the equation. Now, compilation is a separate step.

//const { interface, bytecode} = require('./compile');

const provider = new HDWalletProvider(
    // need to insert my mnemonic here, ,
    // Include the link or network that you want to connect to.
    'minor host shaft pave earth awful category recipe echo small thunder pair',
    'https://rinkeby.infura.io/v3/3ed05ab005bf4d85a60e6264de5574f7'
);

const web3 = new Web3(provider);
// This web3 instance can be used to interact with test network in any way,
// shape, or form.

const deploy = async () => {
    const accounts = await web3.eth.getAccounts();
    console.log('Attempting to deploy from account', accounts[0]);

    const result = await new web3.eth.Contract(JSON.parse(compiledFactory.interface))
        .deploy({data: '0x' + compiledFactory.bytecode}) // Be mindful of what arguments your thing accepts.
        .send({ from: accounts[0], gas: '1000000' });

    console.log('Contract deployed to', result.options.address);


};
deploy();