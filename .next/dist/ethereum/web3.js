'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _web = require('web3');

var _web2 = _interopRequireDefault(_web);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var web3 = void 0; // declares and sets up this variable for future suse.
// the widnow.web3 section fails and breaks on server side version.

if (typeof window !== "undefined" && typeof window.web3 !== 'undefined') {
    // typeof is used to see if variable is defined.
    // if in browser and metamask is running
    web3 = new _web2.default(window.web3.currentProvider); // hijack this thing's provider.
    // if you're in node.js runtime, the variable is undefined.
    // You'll return "object" for the type of window if you're in the browser side.
} else {
    // IF...we are on the server, OR user has no metamask.
    // set up our own provider that connects to the test network through infura.
    var provider = new _web2.default.providers.HttpProvider('https://rinkeby.infura.io/v3/3ed05ab005bf4d85a60e6264de5574f7');
    web3 = new _web2.default(provider);
    // This line here enables you to pass into the new provider that we passed htorugh our unique access key via the
    // Infura portal into the rinkeby test network.
}
// const web3 = new Web3(window.web3.currentProvider);
// window.web3.currentProvider is the provider that is given to us by
// metamask.
// This will throw an error for people without metamask.


// Then you need to make sure that you are exporting this!

exports.default = web3;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImV0aGVyZXVtL3dlYjMuanMiXSwibmFtZXMiOlsiV2ViMyIsIndlYjMiLCJ3aW5kb3ciLCJjdXJyZW50UHJvdmlkZXIiLCJwcm92aWRlciIsInByb3ZpZGVycyIsIkh0dHBQcm92aWRlciJdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsQUFBTzs7Ozs7O0FBRVAsSUFBSSxZLEFBQUosR0FBVTtBQUNWOztBQUVBLElBQUksT0FBQSxBQUFPLFdBQVAsQUFBa0IsZUFBZSxPQUFPLE9BQVAsQUFBYyxTQUFuRCxBQUE0RDtBQUN4RDtBQUNBO1dBQU8sQUFBSSxrQkFBSyxPQUFBLEFBQU8sS0FGOEMsQUFFckUsQUFBTyxBQUFxQixpQkFGeUMsQUFBRSxDQUV6QixBQUM5QztBQUNBO0FBQ0g7QUFMRCxPQUtPLEFBQ0g7QUFDQTtBQUNBO1FBQU0sV0FBVyxJQUFJLGNBQUEsQUFBSyxVQUFULEFBQW1CLGFBQXBDLEFBQWlCLEFBQ3JCLEFBRUk7V0FBTyxBQUFJLGtCQUFYLEFBQU8sQUFBUyxBQUNoQjtBQUNBO0FBRUg7O0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7OztBQUdBLEFBRUE7O2tCQUFBLEFBQWUiLCJmaWxlIjoid2ViMy5qcyIsInNvdXJjZVJvb3QiOiIvVXNlcnMvcTQ2MDI5NC9ia2NoYWluX3BnZS90cmFpbmluZy9raWNrc3RhcnQifQ==