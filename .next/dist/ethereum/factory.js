'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _web = require('./web3');

var _web2 = _interopRequireDefault(_web);

var _CampaignFactory = require('./build/CampaignFactory.json');

var _CampaignFactory2 = _interopRequireDefault(_CampaignFactory);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var instance = new _web2.default.eth.Contract(
// Your contract instance lives in the ABI, which is this .JSON above.
JSON.parse(_CampaignFactory2.default.interface), '0xAfF066Eafd89fcA427b7dF41eC88417eF4183652');

exports.default = instance;

// Why separate some code between web3.js and factory.js?
// If you need to get access to your deployed factory instance, from
// somewhere else inside the application, then you won't have to go through
// the entire process of importing web3.js , the interface, and get the addrewss and all thaat stuff.
// Now, latency is not laggy.
// Instead, to get handl eon insance, you just import the factory.js file.
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImV0aGVyZXVtL2ZhY3RvcnkuanMiXSwibmFtZXMiOlsid2ViMyIsIkNhbXBhaWduRmFjdG9yeSIsImluc3RhbmNlIiwiZXRoIiwiQ29udHJhY3QiLCJKU09OIiwicGFyc2UiLCJpbnRlcmZhY2UiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLEFBQU8sQUFBUCxBQUFpQixBQUFqQjs7OztBQUNBLEFBQU8sQUFBUCxBQUE0QixBQUE1Qjs7Ozs7O0FBRUEsSUFBTSxXQUFXLElBQUksY0FBSyxBQUFMLElBQVMsQUFBYjtBQUNiO0FBQ0EsS0FBSyxBQUFMLE1BQVcsMEJBQWdCLEFBQTNCLEFBRmEsWUFHYixBQUhhLEFBQWpCLEFBTUE7O2tCQUFlLEFBQWY7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImZhY3RvcnkuanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL3E0NjAyOTQvYmtjaGFpbl9wZ2UvdHJhaW5pbmcva2lja3N0YXJ0In0=