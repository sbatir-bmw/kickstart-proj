'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _semanticUiReact = require('semantic-ui-react');

var _routes = require('../routes');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = '/Users/q460294/bkchain_pge/training/kickstart/components/Header.js';

// This is the link helper to go into our routes file.

exports.default = function () {
    return _react2.default.createElement(_semanticUiReact.Menu, { style: { marginTop: '10px' }, __source: {
            fileName: _jsxFileName,
            lineNumber: 7
        }
    }, _react2.default.createElement(_routes.Link, { route: '/', __source: {
            fileName: _jsxFileName,
            lineNumber: 8
        }
    }, _react2.default.createElement('a', { className: 'item', __source: {
            fileName: _jsxFileName,
            lineNumber: 9
        }
    }, 'crowdCoin')), _react2.default.createElement(_semanticUiReact.Menu.Menu, { position: 'right', __source: {
            fileName: _jsxFileName,
            lineNumber: 14
        }
    }, _react2.default.createElement(_routes.Link, { route: '/', __source: {
            fileName: _jsxFileName,
            lineNumber: 15
        }
    }, _react2.default.createElement('a', { className: 'item', __source: {
            fileName: _jsxFileName,
            lineNumber: 16
        }
    }, 'Campaigns')), _react2.default.createElement(_routes.Link, { route: '/campaigns/new', __source: {
            fileName: _jsxFileName,
            lineNumber: 21
        }
    }, _react2.default.createElement('a', { className: 'item', __source: {
            fileName: _jsxFileName,
            lineNumber: 22
        }
    }, '+'))));
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvSGVhZGVyLmpzIl0sIm5hbWVzIjpbIlJlYWN0IiwiTWVudSIsIkxpbmsiLCJtYXJnaW5Ub3AiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLEFBQU87Ozs7QUFDUCxBQUFTOztBQUNULEEsQUFBQSxBQUFTLEFBQVk7Ozs7OztBQUFjLEFBRW5DOztrQkFBZSxZQUFNLEFBQ2pCOzJCQUNJLEFBQUMsdUNBQUssT0FBUyxFQUFFLFdBQWpCLEFBQWUsQUFBYTtzQkFBNUI7d0JBQUEsQUFDSTtBQURKO0tBQUEsa0JBQ0ksQUFBQyw4QkFBSyxPQUFOLEFBQWM7c0JBQWQ7d0JBQUEsQUFDSTtBQURKO3VCQUNJLGNBQUEsT0FBRyxXQUFILEFBQWE7c0JBQWI7d0JBQUE7QUFBQTtPQUZSLEFBQ0ksQUFDSSxBQUtSLCtCQUFDLGNBQUQsc0JBQUEsQUFBTSxRQUFLLFVBQVgsQUFBb0I7c0JBQXBCO3dCQUFBLEFBQ0U7QUFERjt1QkFDRSxBQUFDLDhCQUFLLE9BQU4sQUFBYztzQkFBZDt3QkFBQSxBQUNNO0FBRE47dUJBQ00sY0FBQSxPQUFHLFdBQUgsQUFBYTtzQkFBYjt3QkFBQTtBQUFBO09BRlIsQUFDRSxBQUNNLEFBS1IsK0JBQUEsQUFBQyw4QkFBSyxPQUFOLEFBQWM7c0JBQWQ7d0JBQUEsQUFDUTtBQURSO3VCQUNRLGNBQUEsT0FBRyxXQUFILEFBQWE7c0JBQWI7d0JBQUE7QUFBQTtPQWhCWixBQUNJLEFBT0EsQUFPQSxBQUNRLEFBUWY7QUF6QkQiLCJmaWxlIjoiSGVhZGVyLmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9xNDYwMjk0L2JrY2hhaW5fcGdlL3RyYWluaW5nL2tpY2tzdGFydCJ9