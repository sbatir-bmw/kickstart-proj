const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const web3 = new Web3(ganache.provider());

const compiledFactory = require('../ethereum/build/CampaignFactory.json'); // Pulls in the entire thing
const compiledCampaign = require('../ethereum/build/Campaign.json');

let accounts;
let factory;
let campaignAddress;
let campaign;

beforeEach(async () => {
    // we will use the contract constructor
    // that is part of the web3.eth library.
    accounts = await web3.eth.getAccounts();
    // Contract constructor accepts only JS object, not general
    // JSON. So need to parse JSON and pass to Contract consttructor
    factory = await new web3.eth.Contract(JSON.parse(compiledFactory.interface))
        .deploy({ data: '0x' + compiledFactory.bytecode}) // deploy takes the bytecode.
        .send({from: accounts[0], gas: '1000000'});

    await factory.methods.createCampaign('100').send({
        from: accounts[0],
        gas: '1000000'
    });
    // remember this is a view function... you are not changing any data
    [campaignAddress]= await factory.methods.getDeployedCampaigns().call();
    campaign = await new web3.eth.Contract(
      JSON.parse(compiledCampaign.interface),
      campaignAddress
    );
});

// INitiate the Testing Phase
// Reasoanble first test

describe('Campaigns', () => {
    it ('deploys factory and campaign!', () => { // did I import this successfully
        assert.ok(factory.options.address); // from solidity language to the blockchain?
        assert.ok(campaign.options.address);
    });

    it ('marks caller as the campaign manager', async () => {
       // assert manager was correctly set.
        const manager = await campaign.methods.manager().call();
        assert.equal(accounts[0], manager); // check if the manager and accoutns at zero are same.
    });
    it('allows people to contribute money and marks them as approvers', async() => {
       await campaign.methods.contribute().send({
           value: '200',
           from: accounts[1]
       });
       const isContributor = await campaign.methods.approvers(accounts[1]).call();
       assert(isContributor);
    });
    it ('requires a minimum contribution', async() => {
        try {
            await campaign.methods.contribute().send({
                value: '5',
                from: accounts[1]
            });
            assert(false);
        } catch (err) {
            assert(err);

        }
    });

    it('allows a manager ot make a payment request', async() => {
       await campaign.methods
           .createRequest('Buy batteries', '100', accounts[1])
           .send({
              from: accounts[0],
              gas: '1000000'
           });
       const request = await campaign.methods.requests(0).call();
       assert.equal('Buy batteries', request.description);
    });

    it('processes requests', async() => {
        await campaign.methods.contribute().send({
            from: accounts[0],
            value: web3.utils.toWei('10', 'ether')
        });

        await campaign.methods
            .createRequest('A',web3.utils.toWei('5', 'ether'), accounts[1])
            .send({from: accounts[0], gas: '1000000'});

        await campaign.methods.approveRequest(0).send({
           from: accounts[0],
           gas: '1000000'
        });

        // finalize it
        await campaign.methods.finalizeRequest(0).send({
           from: accounts[0],
           gas: '1000000'
        });

        //
        let balance = await web3.eth.getBalance(accounts[1]); // take string, turn to ether.
        balance = web3.utils.fromWei(balance, 'ether');
        balance = parseFloat(balance); // typecast

        // between every testrun, you redeploy campaign and factory, but we don't know about Money.
        // not doing proper clean-up between tests.
        console.log(balance);
        assert(balance > 104);
    });
});